package be.kdg.hardware.services.api;


import be.kdg.hardware.dom.shopping.Cart;
import be.kdg.hardware.dom.shopping.CartItem;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by wouter on 10/20/15.
 */
@Transactional
public interface CartService {

    Cart findCartById(Integer cartId);

    CartItem findCartItemById(Integer cartItemId);

}
