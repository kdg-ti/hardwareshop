package be.kdg.hardware.services.api;

import be.kdg.hardware.dom.stock.Product;
import be.kdg.hardware.dom.stock.exceptions.StockException;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

/**
 * Created by wouter on 10/6/15.
 */
@Transactional
public interface ProductService {
    Product addProduct(Product product) throws StockException;

    List<Product> findAll();

    Product findProductById(Integer productId) throws StockException;

    Collection<String> getCategories();

    Product getProductByDescription(String productName) throws StockException;

    Collection<Product> getProductsOfCategory(String categoryName);

    Integer getStockCount(Integer productId) throws StockException;

    void removeProduct(Integer productId) throws StockException;

    Product updateProduct(Integer productId, int amount) throws StockException;
}
