package be.kdg.hardware.services.api;


import be.kdg.hardware.dom.shopping.Cart;
import be.kdg.hardware.dom.shopping.Order;
import be.kdg.hardware.dom.user.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by wouter on 10/6/15.
 */
@Transactional
public interface UserService extends UserDetailsService {
    User addUser(User user);

    Order checkOut(Integer userId);

    User findUserById(Integer userId);

    User findUserByUsername(String username);

    List<User> findUsers();

    Cart getCartByUserId(Integer userId);

    Integer getCartItemAmount(Integer userId, Integer productId);

    void removeUser(Integer userId);

    Cart updateCart(Integer userId, Integer productId, int amount);
}
