package be.kdg.hardware.services.impl;

import be.kdg.hardware.dom.shopping.Cart;
import be.kdg.hardware.dom.shopping.CartItem;
import be.kdg.hardware.dom.user.exceptions.UserException;
import be.kdg.hardware.persistence.api.CartItemRepository;
import be.kdg.hardware.persistence.api.CartRepository;
import be.kdg.hardware.services.api.CartService;
import be.kdg.hardware.services.api.ProductService;
import org.springframework.stereotype.Service;

/**
 * Created by wouter on 10/20/15.
 */

@Service
public class CartServiceImpl implements CartService {
    private final CartItemRepository cartItemRepository;
    private final CartRepository cartRepository;
    private final ProductService productService;

    public CartServiceImpl(CartItemRepository cartItemRepository,
                           CartRepository cartRepository, ProductService productService) {
        this.cartItemRepository = cartItemRepository;
        this.cartRepository = cartRepository;
        this.productService = productService;
    }

    @Override
    public Cart findCartById(Integer cartId) {
        Cart cart = cartRepository.findOne(cartId);
        if (cart == null)
            throw new UserException("Cart not found");
        return cart;
    }

    @Override
    public CartItem findCartItemById(Integer cartItemId) {
        CartItem cartItem = cartItemRepository.findOne(cartItemId);
        if (cartItem == null)
            throw new UserException("Cart not found");
        return cartItem;
    }
}
