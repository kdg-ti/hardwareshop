package be.kdg.hardware.services.impl;

import be.kdg.hardware.dom.shopping.Cart;
import be.kdg.hardware.dom.shopping.Order;
import be.kdg.hardware.dom.stock.Product;
import be.kdg.hardware.dom.stock.exceptions.StockException;
import be.kdg.hardware.dom.user.User;
import be.kdg.hardware.dom.user.exceptions.UserException;
import be.kdg.hardware.dom.user.roles.Client;
import be.kdg.hardware.dom.user.roles.Role;
import be.kdg.hardware.persistence.api.ProductRepository;
import be.kdg.hardware.persistence.api.UserRepository;
import be.kdg.hardware.services.api.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final ProductRepository productRepository;


    public UserServiceImpl(UserRepository userRepository,
                           ProductRepository productRepository) {
        this.userRepository = userRepository;
        this.productRepository = productRepository;
    }

    /**
     * Voeg een gebruiker toe aan het systeem.
     *
     * @param user
     */
    public User addUser(User user) {
        return userRepository.save(user);
    }


    /**
     * Maakt een bestelling op basis van de huidige winkelkarinhoud
     *
     * @param userId
     * @return
     */
    public Order checkOut(Integer userId) {
        Client client = getClient(userId);
        return client.createOrder();
    }

    public User findUserById(Integer userId) {
        User user = userRepository.findOne(userId);
        if (user == null)
            throw new UserException("User not found");
        return user;
    }

    /**
     * Geef de gebruiker die hoort bij deze username
     *
     * @param username
     * @return
     */
    public User findUserByUsername(String username) {
        User user = userRepository.findUserByUsername(username);
        if (user == null)
            throw new UserException("User not found");

        return user;
    }

    /**
     * Geef alle gebruikers met gebruikersnaam die het systeem kent
     *
     * @return
     */
    public List<User> findUsers() {
        return userRepository.findAll();
    }

    @Override
    public Cart getCartByUserId(Integer userId) {
        Client client = getClient(userId);
        return client.getCart();
    }

    /**
     * Haalt voor deze gebruiker de aanwezige hoeveelheid product op
     * uit zijn huidige winkelkarretje.  Wordt gebruikt om de hoeveelheden
     * te berekenen die aangepast zullen moeten worden.
     *
     * @param userId
     * @param productId
     * @return
     */
    public Integer getCartItemAmount(Integer userId, Integer productId) {
        Client client = getClient(userId);
        if (client == null)
            throw new UserException("User not found");
        Product product = productRepository.findOne(productId);
        if (product == null)
            throw new StockException("Product not found");
        return client.getCart().getCartItemAmount(product);
    }

    /**
     * Verwijdert een gebruiker op basis van de gebruikersnaam
     *
     * @param userId
     */
    public void removeUser(Integer userId) {
        userRepository.delete(userId);
    }

    /**
     * Pas voor dit product de winkelkarhoeveelheid aan.
     * Dit getal kan ook negatief zijn.
     * De juiste hoeveelheid moet berekend worden.
     *
     * @param userId
     * @param productId
     * @param amount
     */
    public Cart updateCart(Integer userId, Integer productId, int amount) {
        User user = userRepository.findOne(userId);
        Client client = getClient(userId);

        if (client == null)
            throw new UserException("User not found");
        Product product = productRepository.findOne(productId);
        if (product == null)
            throw new StockException("Product not found");

        client.getCart().updateProduct(product, amount);
        userRepository.save(user);
        return client.getCart();
    }

    private Client getClient(Integer userId) {
        User user = userRepository.findOne(userId);
        if (user == null)
            throw new UserException("User not found");

        if (!Role.hasRole(user, Client.class)) {
            throw new UserException("Incorrect role");
        }

        return Role.loadRole(user, Client.class);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findUserByUsername(username);
    }
}