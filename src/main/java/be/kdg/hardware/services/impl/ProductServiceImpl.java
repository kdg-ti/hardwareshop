package be.kdg.hardware.services.impl;

import be.kdg.hardware.dom.stock.Product;
import be.kdg.hardware.dom.stock.StockItem;
import be.kdg.hardware.dom.stock.exceptions.StockException;
import be.kdg.hardware.persistence.api.ProductRepository;
import be.kdg.hardware.services.api.ProductService;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Product findProductById(Integer productId) throws StockException {
        Product product = productRepository.findOne(productId);
        if (product == null)
            throw new StockException("Product not found");
        return product;
    }

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    /**
     * Voeg een product toe aan de catalogus
     *
     * @param product
     * @throws StockException
     */
    public Product addProduct(Product product) throws StockException {
        return productRepository.save(product);
    }

    /**
     * Vraag alle product op van een bepaalde categorie
     *
     * @param categoryName
     * @return
     */
    public Collection<Product> getProductsOfCategory(String categoryName) {
        return productRepository.getProductsByCategoryName(categoryName);
    }

    /**
     * Vraag alle categorieën op die er zijn
     *
     * @return
     */
    public Collection<String> getCategories() {
        return productRepository.getCategories();
    }

    /**
     * Vraag een product op aan de hand van de productbeschrijving
     *
     * @param productName unieke productnaam
     * @return
     */
    @Override
    public Product getProductByDescription(String productName) throws StockException {
        Product product = productRepository.findProductByDescription(productName);
        if (product == null)
            throw new StockException("Product not found");
        return product;
    }


    /**
     * Geeft de stockcount voor een product
     *
     * @param productId
     * @return
     */
    public Integer getStockCount(Integer productId) {
        Product product = productRepository.findOne(productId);
        if (product == null)
            throw new StockException("Product not found");

        return product.getStockItem().getAmount();
    }

    /**
     * Update een product in de catalogus
     *
     * @param productId
     * @throws StockException
     */
    public Product updateProduct(Integer productId, int amount) throws StockException {
        Product product = productRepository.findOne(productId);
        if (product == null)
            throw new StockException("Product not found");

        StockItem item = product.getStockItem();
        int newAmount = item.getAmount() - amount;
        if (newAmount >= 0) {
            item.setAmount(newAmount);
            return productRepository.save(product);
        } else
            throw new StockException("De voorraad voor het product " + product.getDescription() + " bedraagt maar " + item.getAmount());
    }

    public void removeProduct(Integer productId) throws StockException {
        Product product = productRepository.findOne(productId);
        if (product == null)
            throw new StockException("Product not found");

        productRepository.delete(product);
    }
}