package be.kdg.hardware.web.resources.shopping;

import be.kdg.hardware.web.resources.stock.ProductResource;

/**
 * Created by wouter on 10/16/15.
 */
public class CartItemResource {
    private Integer cartItemId;
    private Integer amount;
    private Double total;
    private ProductResource productResource;

    public CartItemResource() {
        this.productResource = new ProductResource();
    }

    public Integer getCartItemId() {
        return cartItemId;
    }

    public void setCartItemId(Integer cartItemId) {
        this.cartItemId = cartItemId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public ProductResource getProductResource() {
        return productResource;
    }

    public void setProductResource(ProductResource productResource) {
        this.productResource = productResource;
    }

    public Double getTotal() {
        return amount * productResource.getPrice();
    }
}
