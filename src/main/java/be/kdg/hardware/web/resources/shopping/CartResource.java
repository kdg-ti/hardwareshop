package be.kdg.hardware.web.resources.shopping;

/**
 * Created by wouter on 10/16/15.
 */
public class CartResource {
    private Integer cartid;
    private Double totalPrice;

    public Integer getCartid() {
        return cartid;
    }

    public void setCartid(Integer cartid) {
        this.cartid = cartid;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }
}
