package be.kdg.hardware.web.resources.shopping;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;

/**
 * Created by wouter on 10/16/15.
 */
public class OrderResource implements Serializable {
    private Integer orderId;

    private Collection<LineItemResource> lineItemResources;

    private LocalDateTime orderDate;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Collection<LineItemResource> getLineItemResources() {
        return lineItemResources;
    }

    public void setLineItemResources(Collection<LineItemResource> lineItemResources) {
        this.lineItemResources = lineItemResources;
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDateTime orderDate) {
        this.orderDate = orderDate;
    }
}
