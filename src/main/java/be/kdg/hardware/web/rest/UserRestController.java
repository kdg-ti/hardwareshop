package be.kdg.hardware.web.rest;

import be.kdg.hardware.dom.shopping.Cart;
import be.kdg.hardware.dom.user.User;
import be.kdg.hardware.services.api.UserService;
import be.kdg.hardware.web.assemblers.CartAssembler;
import be.kdg.hardware.web.assemblers.UserAssembler;
import be.kdg.hardware.web.resources.shopping.CartResource;
import be.kdg.hardware.web.resources.users.UserResource;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by wouter on 10/16/15.
 */
@RestController
@RequestMapping(value = "/api/users")
public class UserRestController {

    private final Logger logger = Logger.getLogger(UserRestController.class);
    private final UserService userService;
    private final UserAssembler userAssembler;
    private final CartAssembler cartAssembler;

    public UserRestController(UserService userService,
                              UserAssembler userAssembler,
                              CartAssembler cartAssembler) {
        this.userService = userService;
        this.userAssembler = userAssembler;
        this.cartAssembler = cartAssembler;
    }

    @PostMapping
    public ResponseEntity<UserResource> createUser(@Valid @RequestBody UserResource userResource) {
        User user_in = userAssembler.fromResource(userResource);
        User user_out = userService.addUser(user_in);

        logger.info(this.getClass().toString() + ": adding new user " + user_out.getUserId());
        return new ResponseEntity<>(userAssembler.toResource(user_out), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<UserResource>> findAll() {
        List<User> users = userService.findUsers();

        List<UserResource> userResources = users.stream().map(u -> userAssembler.toResource(u)).collect(Collectors.toList());
        return new ResponseEntity<>(userResources, HttpStatus.OK);
    }

    @GetMapping("/{userId}")
    public ResponseEntity<UserResource> findUserById(@PathVariable int userId) {

        logger.info(this.getClass().toString() + ":" + userId);
        User user = userService.findUserById(userId);
        UserResource userResource = userAssembler.toResource(user);
        return new ResponseEntity<>(userResource, HttpStatus.OK);
    }

    @GetMapping("/{userId}/cart")
    public ResponseEntity<CartResource> findCartByUserId(@PathVariable Integer userId) {
        Cart cart = userService.getCartByUserId(userId);
        CartResource cartResource = cartAssembler.toResource(cart);
        return new ResponseEntity<>(cartResource, HttpStatus.OK);
    }

    @GetMapping("/{userId}/cart/product/{productId}")
    public ResponseEntity<CartResource> updateCart(@PathVariable Integer userId, @PathVariable Integer productId, @RequestParam(value = "amount") Integer amount) {
        Cart cart = userService.updateCart(userId, productId, amount);
        CartResource cartResource = cartAssembler.toResource(cart);
        return new ResponseEntity<>(cartResource, HttpStatus.OK);
    }
}
