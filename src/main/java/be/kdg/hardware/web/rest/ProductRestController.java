package be.kdg.hardware.web.rest;

import be.kdg.hardware.dom.stock.Product;
import be.kdg.hardware.services.api.ProductService;
import be.kdg.hardware.web.assemblers.ProductAssembler;
import be.kdg.hardware.web.resources.stock.ProductResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by wouter on 10/20/15.
 */
@RestController
@RequestMapping(value = "/api/products")
public class ProductRestController {

    private final ProductService productService;
    private final ProductAssembler productAssembler;

    public ProductRestController(ProductService productService, ProductAssembler productAssembler) {
        this.productService = productService;
        this.productAssembler = productAssembler;
    }

    @PostMapping
    public ResponseEntity<ProductResource> createProduct(@Valid @RequestBody ProductResource productResource) {
        Product product_in = productAssembler.fromResource(productResource);
        Product product_out = productService.addProduct(product_in);
        return new ResponseEntity<>(productAssembler.toResource(product_out), HttpStatus.CREATED);
    }

    @GetMapping("/{productId}")
    public ResponseEntity<ProductResource> findProductById(@PathVariable Integer productId) {
        Product product = productService.findProductById(productId);
        return new ResponseEntity<>(productAssembler.toResource(product), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<ProductResource>> findAll() {
        List<Product> products = productService.findAll();
        return new ResponseEntity<>(productAssembler.toResources(products), HttpStatus.OK);
    }

    @GetMapping("/{productId}/{amount}")
    public ResponseEntity<ProductResource> updateStockCount(@PathVariable Integer productId, @PathVariable Integer amount) {
        Product product = productService.updateProduct(productId, amount);
        return new ResponseEntity<>(productAssembler.toResource(product), HttpStatus.OK);
    }

    @DeleteMapping("/{productId}")
    public ResponseEntity<ProductResource> deleteProductById(@PathVariable Integer productId) {
        productService.removeProduct(productId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


}
