package be.kdg.hardware.web.rest;

import be.kdg.hardware.dom.shopping.CartItem;
import be.kdg.hardware.services.api.CartService;
import be.kdg.hardware.web.assemblers.CartItemAssembler;
import be.kdg.hardware.web.resources.shopping.CartItemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wouter on 10/16/15.
 */
@RestController
@RequestMapping(value = "/api/cartitems")
public class CartItemRestController {

    private final CartService cartService;
    private final CartItemAssembler cartItemAssembler;

    public CartItemRestController(CartService cartService, CartItemAssembler cartItemAssembler) {
        this.cartService = cartService;
        this.cartItemAssembler = cartItemAssembler;
    }

    @GetMapping("/{cartItemId}")
    public ResponseEntity<CartItemResource> findCartItemByCartId(@PathVariable Integer cartItemId) {
        CartItem cartItem = cartService.findCartItemById(cartItemId);
        CartItemResource cartItemResource = cartItemAssembler.toResource(cartItem);
        return new ResponseEntity<>(cartItemResource, HttpStatus.OK);
    }
}
