package be.kdg.hardware.web.rest;

import be.kdg.hardware.dom.shopping.Cart;
import be.kdg.hardware.services.api.CartService;
import be.kdg.hardware.web.assemblers.CartAssembler;
import be.kdg.hardware.web.resources.shopping.CartResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wouter on 10/16/15.
 */
@RestController
@RequestMapping(value = "/api/carts")
public class CartRestController {

    private final CartService cartService;
    private final CartAssembler cartAssembler;

    public CartRestController(CartService cartService, CartAssembler cartAssembler) {
        this.cartService = cartService;
        this.cartAssembler = cartAssembler;
    }

    @GetMapping("/{cartId}")
    public ResponseEntity<CartResource> findCartByCartId(@PathVariable Integer cartId) {
        Cart cart = cartService.findCartById(cartId);
        CartResource cartResource = cartAssembler.toResource(cart);
        return new ResponseEntity<>(cartResource, HttpStatus.OK);
    }
}
