package be.kdg.hardware.dom.shopping;

import be.kdg.hardware.dom.stock.Product;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class CartItem implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "CART_ITEM_ID", nullable = false)
    private Integer cartItemId;

    @ManyToOne(targetEntity = Product.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "PRODUCT_ID", nullable = false)
    private Product product;

    @ManyToOne(targetEntity = Cart.class, fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "CART_ID", nullable = false)
    private Cart cart;

    @Column(name = "AMOUNT", nullable = false)
    private Integer amount;

    public CartItem() {
        this.amount = 0;
    }

    public CartItem(Cart cart, Product product, Integer amount) {
        this.cart = cart;
        this.product = product;
        this.amount = amount;
    }

    public Integer getCartItemId() {
        return cartItemId;
    }

    public void setCartItemId(Integer cartItemId) {
        this.cartItemId = cartItemId;
    }

    public Double getPrice() {
        return this.product.getPrice() * this.getAmount();
    }

    public Integer getAmount() {
        return this.amount;
    }

    public Product getProduct() {
        return this.product;
    }
}