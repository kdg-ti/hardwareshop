package be.kdg.hardware.dom.shopping;

import be.kdg.hardware.dom.user.roles.Client;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "`Order`") // Name convention leads to error because of H2 'Order' keyword
public class Order implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "ORDER_ID", nullable = false)
    private Integer orderId;

    @OneToMany(targetEntity = LineItem.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "order")
    private Collection<LineItem> lineItems;

    @Column(name = "DATE", nullable = false)
    private LocalDateTime orderDate;

    @ManyToOne(targetEntity = Client.class, fetch = FetchType.EAGER, optional = false)
    private Client client;

    public Order() {
        orderDate = LocalDateTime.now();
        lineItems = new ArrayList<>();
    }

    public Integer getId() {
        return orderId;
    }

    /**
     * Bereken de totale prijs voor die order.
     *
     * @return
     */
    public double getTotalPrice() {
        double totalPrice = 0.0d;
        for (LineItem lineItem : lineItems)
            totalPrice += lineItem.getPrice();
        return totalPrice;
    }

    /**
     * @return
     */
    public LocalDateTime getOrderDate() {
        return this.orderDate;
    }

    public Collection<LineItem> getLineItems() {
        return lineItems;
    }

    public synchronized void addLineItem(LineItem lineItem) {
        this.lineItems.add(lineItem);
    }

}