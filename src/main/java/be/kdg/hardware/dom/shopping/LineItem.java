package be.kdg.hardware.dom.shopping;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class LineItem implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "LINE_ITEM_ID", nullable = false)
    private Integer lineItemId;

    @Column(name = "DESCRIPTION", nullable = false, length = 255)
    private String productDescription;

    @Column(name = "AMOUNT", nullable = false)
    private int amount;

    @Column(name = "PRICE", nullable = false)
    private double price;

    @ManyToOne(targetEntity = Order.class)
    private Order order;

    public LineItem() {
    }

    public LineItem(String productDescription, int amount, double price) {
        this.productDescription = productDescription;
        this.amount = amount;
        this.price = price;
    }

    public Integer getLineItemId() {
        return lineItemId;
    }

    public void setLineItemId(Integer lineItemId) {
        this.lineItemId = lineItemId;
    }

    /**
     * @return
     */
    public int getAmount() {
        return amount;
    }

    /**
     * @return
     */
    public double getPrice() {
        return price;

    }

    public String getProductDescription() {
        return productDescription;
    }


}