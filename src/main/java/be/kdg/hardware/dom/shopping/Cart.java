package be.kdg.hardware.dom.shopping;


import be.kdg.hardware.dom.stock.Product;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Entity
public class Cart implements Serializable {
    @Column(name = "CART_ID", nullable = false)
    @Id
    @GeneratedValue
    private Integer cartid;

    @OneToMany(targetEntity = CartItem.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "cart")
    private Map<Integer, CartItem> cartItems;

    @Transient
    private Double totalPrice;

    public Cart() {
        this.cartItems = new ConcurrentHashMap<>();
    }

    public Integer getId() {
        return cartid;
    }

    public Integer getCartid() {
        return cartid;
    }

    public void setCartid(Integer cartid) {
        this.cartid = cartid;
    }

    public Map<Integer, CartItem> getCartItems() {
        return this.cartItems;
    }

    public void setCartItems(Map<Integer, CartItem> cartItems) {
        this.cartItems = cartItems;
    }

    /**
     * Hiermee kan je een product toevoegen of verwijderen uit het
     * winkelkarretje.
     *
     * @param amount  kan dus ook een negatieve getal zijn.
     * @param product
     * @param amount
     */
    public void updateProduct(Product product, final Integer amount) {
        CartItem cartItem;

        if (cartItems.keySet().contains(product.getProduct_id())) {
            cartItem = cartItems.get(product.getProduct_id());
            int newAmount = cartItem.getAmount() + amount;
            if (newAmount > 0) {
                // cartItem is final daarom constructor aanroepen
                cartItem = new CartItem(this, product, newAmount);
                cartItems.put(product.getProduct_id(), cartItem);
            } else
                cartItems.remove(product.getProduct_id());
        } else {
            cartItem = new CartItem(this, product, amount);
            cartItems.put(product.getProduct_id(), cartItem);
        }
    }

    public Double getTotalPrice() {
        Double sum = 0.0d;
        for (CartItem cartItem : cartItems.values()) {
            sum += cartItem.getPrice();
        }
        totalPrice = sum;
        return sum;
    }

    public int getCartItemAmount(Product product) {
        if (cartItems.containsKey(product.getProduct_id()))
            return this.cartItems.get(product.getProduct_id()).getAmount();
        return 0;
    }

    public void clearCart() {
        cartItems.clear();
    }

}