package be.kdg.hardware.dom.stock;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class StockItem implements Serializable {
    @Id
    @GeneratedValue
    @Column(nullable = false)
    private Integer stockItemId;

    @Column
    private Integer amount;

    @OneToOne(targetEntity = Product.class)
    @JoinColumn(name = "MY_PRODUCT_ID")
    private Product product;

    public StockItem() {
    }

    public StockItem(Integer amount) {
        this.amount = amount;
    }

    public Integer getId() {
        return stockItemId;
    }

    /**
     * Geef de hoeveelheid weer voor dit stockitem
     *
     * @return
     */
    public Integer getAmount() {
        return this.amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Product getProduct() {
        return product;
    }
}