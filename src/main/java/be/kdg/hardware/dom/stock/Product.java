package be.kdg.hardware.dom.stock;


import be.kdg.hardware.dom.shopping.CartItem;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
public class Product implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "PRODUCT_ID", nullable = false)
    private Integer product_id;

    @Column(name = "Description", nullable = false)
    private String description;

    @Column(name = "Price", nullable = false)
    private Double price;

    @Column(name = "Category", nullable = false)
    private String categoryName;

    @OneToOne(targetEntity = StockItem.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "product")
    private StockItem stockItem;

    @OneToMany(targetEntity = CartItem.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "product")
    private Collection<CartItem> cartItem;

    /**
     * Create a product based on a description, a price and a category name.
     * This also initializes a StockItem object for the product.
     *
     * @param description
     * @param price
     * @param categoryName
     */
    public Product(String description, Double price, String categoryName) {
        this.description = description;
        this.price = price;
        this.categoryName = categoryName;
        this.stockItem = new StockItem();
    }

    /**
     * Default constructor is needed for ORM
     */
    public Product() {
    }

    /**
     * Geeft de beschrijving van een product
     *
     * @return
     */
    public String getDescription() {
        return this.description;
    }

    public synchronized void setDescription(String description) {
        this.description = description;
    }

    /**
     * Geeft de prijs van een product
     *
     * @return
     */
    public Double getPrice() {
        return this.price;
    }

    public synchronized void setPrice(Double price) {
        this.price = price;
    }

    /**
     * Geeft de categorie van een product
     *
     * @return
     */
    public String getCategoryName() {
        return categoryName;
    }

    public synchronized void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    /**
     * Geeft het unieke product_id terug.
     *
     * @return
     */
    public Integer getProduct_id() {
        return product_id;
    }

    public synchronized void setProduct_id(Integer product_id) {
        this.product_id = product_id;
    }

    public StockItem getStockItem() {
        return stockItem;
    }

    public synchronized void setStockItem(StockItem stockItem) {
        this.stockItem = stockItem;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + this.product_id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Product other = (Product) obj;
        return this.product_id == other.product_id;
    }
}