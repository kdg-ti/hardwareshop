/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.kdg.hardware.persistence.api;

import be.kdg.hardware.dom.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author wouter
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    User findUserByUsername(String username);

}
