/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.kdg.hardware.persistence.api;

import be.kdg.hardware.dom.stock.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * @author wouter
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

    Product findProductByDescription(String description);

    Collection<Product> getProductsByCategoryName(String categoryName);

    @Query(value = "SELECT distinct p.categoryName from Product p")
    Collection<String> getCategories();
}
