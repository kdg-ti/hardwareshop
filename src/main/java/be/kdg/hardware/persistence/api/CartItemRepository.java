package be.kdg.hardware.persistence.api;

import be.kdg.hardware.dom.shopping.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by wouter on 10/20/15.
 */
@Repository
public interface CartItemRepository extends JpaRepository<CartItem, Integer> {

}
