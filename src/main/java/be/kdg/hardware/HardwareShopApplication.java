package be.kdg.hardware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HardwareShopApplication {

    public static void main(String[] args) {
        SpringApplication.run(HardwareShopApplication.class, args);
    }
}
