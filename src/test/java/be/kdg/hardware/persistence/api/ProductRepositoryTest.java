package be.kdg.hardware.persistence.api;

import be.kdg.hardware.dom.stock.Product;
import be.kdg.hardware.dom.stock.exceptions.StockException;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.Collection;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.Is.is;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductRepositoryTest {

    @Autowired
    private ProductRepository productRepository;

    @Test
    public void addProduct() {
        Product product = new Product("Dummy for Dummies", 4.2, "Dummies");
        Product save = productRepository.save(product);
        assertThat(save.getProduct_id(), is(notNullValue()));
    }

    @Test
    public void findProductByDescription() {
        Product set_weerstanden = productRepository.findProductByDescription("Set weerstanden");
        assertThat(set_weerstanden.getDescription(), is("Set weerstanden"));
    }

    @Test
    public void getProductsByCategoryName() {
        String category = "Magneten";
        Collection<Product> productsOfCategory = productRepository.getProductsByCategoryName(category);
        productsOfCategory.stream().forEach(product -> assertThat(product.getCategoryName(), is(category)));
    }

    @Test
    public void getCategories() {
        Collection<String> categories = productRepository.getCategories();
        assertThat(categories, Matchers.is(not(empty())));
    }

    @Test(expected = StockException.class)
    @Transactional
    public void testRemoveProduct() {
        Product p1 = productRepository.findOne(1);
        productRepository.delete(p1);
        Product p2 = productRepository.findOne(1);
        if (p2 == null)
            throw new StockException("Product niet gevonden");
    }
}