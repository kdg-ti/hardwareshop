package be.kdg.hardware.services.api;

import be.kdg.hardware.dom.shopping.Cart;
import be.kdg.hardware.dom.shopping.Order;
import be.kdg.hardware.dom.stock.Product;
import be.kdg.hardware.dom.user.Address;
import be.kdg.hardware.dom.user.Person;
import be.kdg.hardware.dom.user.User;
import be.kdg.hardware.dom.user.exceptions.UserException;
import be.kdg.hardware.dom.user.roles.Adminstrator;
import be.kdg.hardware.dom.user.roles.Role;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@Transactional
@SpringBootTest
@RunWith(SpringRunner.class)
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private ProductService productService;

    @Test
    public void testAddUser() {
        Address address = new Address("Internationalestraat", "5", "2000", "Antwerpen");
        Person person = new Person("James", "Hetfield", address);
        List<Role> roles = new ArrayList<>();
        roles.add(new Adminstrator());
        User user = new User(person, "james.hetfield@metallica.com", "james", roles);

        userService.addUser(user);
        User james = userService.findUserByUsername("james.hetfield@metallica.com");
        assertThat(james, is(equalTo(user)));
    }

    @Test(expected = UserException.class)
    public void testRemoveUser() {
        String username = "clarence.ho@gmail.com";
        User wouter = userService.findUserByUsername(username);
        userService.removeUser(wouter.getUserId());
        userService.findUserByUsername(username);
    }

    @Test
    public void testUpdateCart() {
        Product product = productService.findProductById(1);
        Cart cart = userService.getCartByUserId(1);
        int cartItemAmount = cart.getCartItemAmount(product);
        userService.updateCart(1, 1, 20);
        Cart cartByUserId = userService.getCartByUserId(1);
        assertThat(cartByUserId.getCartItemAmount(product), equalTo(cartItemAmount + 20));
    }

    @Test
    public void testCheckOut() {
        Order order = userService.checkOut(1);
        assertThat(order.getLineItems(), is(not(empty())));
    }

    @Test
    public void testGetCartItemAmount() {
        Integer cartItemAmount = userService.getCartItemAmount(1, 1);
        assertThat(cartItemAmount, is(greaterThan(0)));
    }

    @Test
    public void testFindUserById() {
        User user = userService.findUserById(3);
        assertThat(user.getUserId(), equalTo(3));
    }

    @Test
    public void testFindUsers() {
        List<User> users = userService.findUsers();
        assertThat(users, is(not(empty())));
    }
}
