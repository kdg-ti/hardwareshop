package be.kdg.hardware.services.api;

import be.kdg.hardware.dom.stock.Product;
import be.kdg.hardware.dom.stock.exceptions.StockException;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@Transactional
@SpringBootTest
@RunWith(SpringRunner.class)
public class ProductServiceTest {

    @Autowired
    private ProductService productService;

    @Test
    public void testAddProduct() {
        Product product = productService.addProduct(new Product("Rode LED", 1.45, "Diodes"));
        assertThat(product.getProduct_id(), isA(Integer.class));
    }

    @Test
    public void testGetProductsOfCategory() {
        String category = "Magneten";
        Collection<Product> productsOfCategory = productService.getProductsOfCategory(category);
        assertThat(productsOfCategory, Matchers.is(not(empty())));
    }

    @Test
    public void testGetCategories() {
        Collection<String> categories = productService.getCategories();
        assertThat(categories, Matchers.is(not(empty())));
    }

    @Test
    public void testGetProductByDescription() {
        String description = "Ronde ferrietmagneet 10 mm";
        Product productByDescription = productService.getProductByDescription(description);
        assertThat(productByDescription.getDescription(), Matchers.equalTo(description));
    }

    @Test
    public void testGetStockCount() {
        String description = "Ronde ferrietmagneet 10 mm";
        Product productByDescription = productService.getProductByDescription(description);
        Integer stockCount = productService.getStockCount(productByDescription.getProduct_id());
        assertThat(stockCount, Matchers.equalTo(50));
    }

    @Test
    public void testUpdateProduct() {
        Integer stockCount = productService.getStockCount(1);
        Integer newCount = 500;
        Product product = productService.updateProduct(1, -newCount);
        assertThat(product.getStockItem().getAmount(), Matchers.equalTo(stockCount + newCount));
    }

    @Test(expected = StockException.class)
    @Transactional
    public void testRemoveProduct() {
        productService.removeProduct(1);
        productService.findProductById(1);
    }
}