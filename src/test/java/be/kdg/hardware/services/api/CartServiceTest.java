package be.kdg.hardware.services.api;

import be.kdg.hardware.dom.shopping.Cart;
import be.kdg.hardware.dom.shopping.CartItem;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CartServiceTest {

    @Autowired
    private CartService cartService;

    @Test
    public void findCartById() {
        Cart cartById = cartService.findCartById(1);
        assertThat(cartById.getCartid(), is(equalTo(1)));
    }

    @Test
    public void findCartItemById() {
        CartItem cartItemById = cartService.findCartItemById(1);
        assertThat(cartItemById.getCartItemId(), is(equalTo(1)));
    }
}