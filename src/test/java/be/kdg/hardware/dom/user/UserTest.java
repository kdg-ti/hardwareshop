package be.kdg.hardware.dom.user;

import be.kdg.hardware.dom.shopping.Order;
import be.kdg.hardware.dom.stock.Product;
import be.kdg.hardware.dom.user.exceptions.UserException;
import be.kdg.hardware.dom.user.roles.Client;
import be.kdg.hardware.dom.user.roles.Role;
import be.kdg.hardware.persistence.api.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserTest {

    private Person person;
    private Role role;
    private Client client;
    @Autowired
    private UserRepository userRepository;

    @Before
    public void runBeforeEachTest() {
        person = mock(Person.class);
        role = mock(Role.class);
        client = new Client();
    }

    @Test
    public void createUserWithRoleClient() {
        List<Role> roles = new ArrayList<>();
        roles.add(client);

        User user = new User(person, "wouter.deketelaere@kdg.be", "wouter", roles);
        assertThat(user.getRoles(), hasItem(isA(Client.class)));
    }

    @Test
    public void createOrder() {
        User firstUser = userRepository.findOne(1);
        if (Role.hasRole(firstUser, Client.class)) {
            Client client = Role.loadRole(firstUser, Client.class);
            int currentSize = client.getCart().getCartItems().size();
            Product product = new Product("Vijzen", 3.14, "Ijzerwaren");
            client.getCart().updateProduct(product, 10);
            Order order = client.createOrder();
            assertThat(order.getLineItems(), hasSize(currentSize + 1));
        } else
            throw new UserException("User is not a client");

    }
}